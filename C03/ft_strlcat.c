/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/17 09:56:03 by alouis            #+#    #+#             */
/*   Updated: 2019/08/21 09:26:06 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

unsigned int	ft_dest_size(char *dest, unsigned int size)
{
	unsigned int	i;

	i = 0;
	while (dest[i] != '\0' && (i < size))
	{
		i++;
	}
	return (i);
}

unsigned int	ft_src_size(char *src)
{
	unsigned int	j;

	j = 0;
	while (src[j] != '\0')
	{
		j++;
	}
	return (j);
}

char			*ft_destok(char *dest, char *src, unsigned int i,
		unsigned int size)
{
	unsigned int	j;

	j = 0;
	while ((src[j] != '\0') && (i < size - 1))
	{
		dest[i] = src[j];
		i++;
		j++;
	}
	dest[i] = '\0';
	return (dest);
}

unsigned int	ft_strlcat(char *dest, char *src, unsigned int size)
{
	unsigned int	i;
	unsigned int	destsize;
	unsigned int	srcsize;
	unsigned int	result;

	if (size == 0)
		return (ft_src_size(src));
	else
	{
		i = 0;
		destsize = ft_dest_size(dest, size);
		srcsize = ft_src_size(src);
		result = destsize + srcsize;
		i = destsize;
		if (dest[i] == '\0')
		{
			ft_destok(dest, src, i, size);
		}
		return (result);
	}
}
