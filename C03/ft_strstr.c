/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/07 14:46:28 by alouis            #+#    #+#             */
/*   Updated: 2019/08/17 09:53:15 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strstr(char *str, char *to_find)
{
	int		i;
	int		j;
	char	*temp;

	i = 0;
	if (to_find[i] == '\0')
		return (str);
	while (str[i] != '\0')
	{
		j = 0;
		if (str[i] == to_find[j])
		{
			temp = &str[i];
			while ((str[i + j] == to_find[j]) && to_find[j] != '\0')
			{
				j++;
				if (to_find[j] == '\0')
					return (temp);
			}
		}
		i++;
	}
	return (0);
}
