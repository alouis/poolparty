/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fibonacci.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/12 11:40:24 by alouis            #+#    #+#             */
/*   Updated: 2019/08/12 12:26:56 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_fibonacci(int index)
{
	int result;

	if (index < 0)
		return (-1);
	if (index == 0)
		result = 0;
	if (index == 1)
		result = 1;
	if (index > 1)
		result = ft_fibonacci(index - 1) + ft_fibonacci(index - 2);
	return (result);
}
