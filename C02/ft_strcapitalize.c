/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcapitalize.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/09 12:53:55 by alouis            #+#    #+#             */
/*   Updated: 2019/08/09 14:14:24 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_low_to_up(char *str, int i)
{
	if (str[i] > 96 && str[i] < 123)
		str[i] = str[i] - 32;
}

void	ft_up_to_low(char *str, int i)
{
	if (str[i] > 64 && str[i] < 91)
		str[i] = str[i] + 32;
}

char	*ft_strcapitalize(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		if ((str[i] >= 'a' && str[i] <= 'z') || (str[i] >= 'A' && str[i] <= 'Z')
				|| (str[i] >= '0' && str[i] <= '9'))
		{
			ft_low_to_up(str, i++);
			while ((str[i] >= 'a' && str[i] <= 'z') || (str[i] >= 'A' &&
							str[i] <= 'Z') || (str[i] >= '0' && str[i] <= '9'))
				ft_up_to_low(str, i++);
		}
		else
			i++;
	}
	return (str);
}
