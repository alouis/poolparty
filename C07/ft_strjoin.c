/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/14 15:50:36 by alouis            #+#    #+#             */
/*   Updated: 2019/08/22 18:00:10 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_strlen(char *str)
{
	int i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

void	ft_concat(int size, char **strs, char *sep, char *dest)
{
	int i;
	int j;
	int k;

	i = 0;
	k = 0;
	while (i < size)
	{
		j = 0;
		while (strs[i][j])
		{
			dest[k] = strs[i][j];
			j++;
			k++;
		}
		j = 0;
		while (sep[j] && i < (size - 1))
		{
			dest[k] = sep[j];
			j++;
			k++;
		}
		i++;
	}
	dest[k] = '\0';
}

char	*ft_strjoin(int size, char **strs, char *sep)
{
	int		i;
	int		size_all_stringsep;
	char	*result_tab;

	result_tab = NULL;
	i = 0;
	size_all_stringsep = 0;
	if (size == 0)
	{
		if (!(result_tab = (char *)malloc(sizeof(char) * 1)))
			return (NULL);
		result_tab[0] = '\0';
		return (result_tab);
	}
	while (i < size)
		size_all_stringsep = size_all_stringsep + ft_strlen(strs[i++]);
	size_all_stringsep = size_all_stringsep + ft_strlen(sep) * (size - 1);
	if (!(result_tab = (char *)malloc(sizeof(char) * (size_all_stringsep + 1))))
		return (0);
	ft_concat(size, strs, sep, result_tab);
	result_tab[size_all_stringsep] = '\0';
	return (result_tab);
}
