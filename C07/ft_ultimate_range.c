/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ultimate_range.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/14 10:51:15 by alouis            #+#    #+#             */
/*   Updated: 2019/08/18 17:55:52 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_ultimate_range(int **range, int min, int max)
{
	unsigned int i;
	unsigned int diff;

	diff = max - min;
	if (min >= max)
	{
		*range = NULL;
		return (0);
	}
	*range = (int *)malloc(sizeof(int) * (diff));
	if (!range)
		return (-1);
	i = 0;
	while (i < diff)
	{
		range[0][i] = min;
		i++;
		min++;
	}
	return (diff);
}
