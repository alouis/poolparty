/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/19 11:18:18 by alouis            #+#    #+#             */
/*   Updated: 2019/08/20 15:41:29 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_strlen(char *str)
{
	int i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

int		ft_search(char c, char *charset)
{
	int i;

	i = 0;
	while (charset[i])
	{
		if (charset[i] == c)
			return (1);
		i++;
	}
	return (0);
}

int		ft_cw(char *str, char *charset)
{
	int	i;
	int	wc;
	int	flag;

	i = 0;
	wc = 0;
	if (ft_search(str[0], charset) == 0 && str[i])
		wc++;
	if (!(*charset) && *str)
		return (1);
	while (str[i])
	{
		flag = 0;
		while (ft_search(str[i], charset) == 1 && str[i])
		{
			i++;
			flag = 1;
		}
		if (flag == 1 && str[i] != '\0')
			wc++;
		i++;
	}
	return (wc);
}

char	*ft_malloc_word(char *str, char *charset)
{
	int		i;
	char	*word;

	i = 0;
	while (ft_search(str[i], charset) == 0 && str[i])
		i++;
	word = (char *)malloc(sizeof(char) * (i + 1));
	if (!(word))
		return (NULL);
	i = 0;
	while (ft_search(str[i], charset) == 0 && str[i])
	{
		word[i] = str[i];
		i++;
	}
	word[i] = '\0';
	return (word);
}

char	**ft_split(char *str, char *charset)
{
	int		i;
	char	**tab;

	if (!(tab = (char **)malloc(sizeof(char *) * (ft_cw(str, charset) + 1))))
		return (NULL);
	i = 0;
	while (*str)
	{
		while (ft_search(*str, charset) == 1 && *str)
			str++;
		if (ft_search(*str, charset) == 0 && *str)
		{
			tab[i] = ft_malloc_word(str, charset);
			i++;
			while (ft_search(*str, charset) == 0 && *str)
				str++;
		}
	}
	tab[i] = 0;
	return (tab);
}
