/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush01.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alongcha <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/03 19:21:19 by alongcha          #+#    #+#             */
/*   Updated: 2019/08/04 19:26:55 by alongcha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	ft_printtop(int x)
{
	int k;

	k = 1;
	ft_putchar('A');
	k++;
	if (x > 1)
	{
		while (k != x)
		{
			ft_putchar('B');
			k++;
		}
		if (k == x)
		{
			ft_putchar('C');
		}
	}
}

void	ft_printmid(int x)
{
	int l;

	l = 1;
	ft_putchar('B');
	l++;
	if (x > 1)
	{
		while (l != x)
		{
			ft_putchar(' ');
			l++;
		}
		if (l == x)
		{
			ft_putchar('B');
		}
	}
}

void	ft_printbot(int x)
{
	int m;

	m = 1;
	ft_putchar('A');
	m++;
	if (x > 1)
	{
		while (m != x)
		{
			ft_putchar('B');
			m++;
		}
	}
	if (m == x)
	{
		ft_putchar('C');
	}
}

void	rush(int x, int y)
{
	int i;

	if (x <= 0 || y <= 0)
		return ;
	ft_printtop(x);
	ft_putchar('\n');
	i = 1;
	if (y > 2)
	{
		while (i < y - 1)
		{
			ft_printmid(x);
			ft_putchar('\n');
			i++;
		}
	}
	if (y > 1)
	{
		ft_printbot(x);
		ft_putchar('\n');
	}
}
